/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */

static int instant = 1;
static int topbar = 1;                      /* -b  option; if 0, dmenu appears at bottom     */
static int fuzzy = 1;                      /* -F  option; if 0, dmenu doesn't use fuzzy matching     */

/* -fn option overrides fonts[0]; default X11 font or font set */
static char font[] = "custom:size=10";
static const char *fonts[] = {
	font,
	"monospace:size=10",
};
static const char *prompt      = NULL;      /* -p  option; prompt to the left of input field */
static const char *symbol_1 = "<";
static const char *symbol_2 = ">";
static int dmx = 0; // x offset
static int dmy = 0; // y offset
static unsigned int dmw = 0; // width
static char normfgcolor[] = "#bbbbbb";
static char normbgcolor[] = "#222222";
static char selfgcolor[]  = "#eeeeee";
static char selbgcolor[]  = "#005577";
static char normhlfgcolor[] = "#ffc978";
static char normhlbgcolor[] = "#222222";
static char selhlfgcolor[]  = "#ffc978";
static char selhlbgcolor[]  = "#005577";
static char midfgcolor[] = "#eeeeee";
static char midbgcolor[] = "#770000";
static char outfgcolor[] = "#000000";
static char outbgcolor[] = "#00ffff";
static const char *colors[SchemeLast][2] = {
								/*     fg         bg       */
	[SchemeNorm] = { normfgcolor, normbgcolor },
	[SchemeSel] = { selfgcolor, selbgcolor },
	[SchemeSelHighlight] = { selhlfgcolor, selhlbgcolor  },
	[SchemeNormHighlight] = { normhlfgcolor, normhlbgcolor },
	[SchemeOut] = { outfgcolor, outbgcolor },
	[SchemeMid] = { midfgcolor, midbgcolor },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;
/* -h option; minimum height of a menu line */
static unsigned int lineheight = 0;
static unsigned int min_lineheight = 8;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
	{ "font",        STRING, &font },
	{ "normfgcolor", STRING, &normfgcolor },
	{ "normbgcolor", STRING, &normbgcolor },
	{ "selfgcolor",  STRING, &selfgcolor },
	{ "selbgcolor",  STRING, &selbgcolor },
	{ "normhlfgcolor", STRING, &normhlfgcolor },
	{ "normhlbgcolor", STRING, &normhlbgcolor },
	{ "selhlfgcolor",  STRING, &selhlfgcolor },
	{ "selhlbgcolor",  STRING, &selhlbgcolor },
	{ "midfgcolor", 	STRING, &midfgcolor },
	{ "midbgcolor",  STRING, &midbgcolor },
	{ "outfgcolor", 	STRING, &outfgcolor },
	{ "outbgcolor",  STRING, &outbgcolor },
	{ "prompt",      STRING, &prompt },
	{ "symbol_1", 	 STRING, &symbol_1 },
	{ "symbol_2", 	 STRING, &symbol_2 },
	{ "instant", 		 INTEGER, &instant },
	{ "topbar", 		 INTEGER, &topbar },
	{ "fuzzy", 			 INTEGER, &fuzzy },
	{ "lines", 			 INTEGER, &lines },
	{ "lineheight",  INTEGER, &lineheight },
	{ "dmx", 				 INTEGER, &dmx },
	{ "dmy", 				 INTEGER, &dmy },
	{ "dmw", 				 INTEGER, &dmw },
	{ "worddelimiters", STRING, &worddelimiters },
};

